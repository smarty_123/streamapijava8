import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;


// also uses Supplier, Predicate, Consumer, Function, and Collectors, Collector
public class StreamAPI {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("Tarun","Yashy","Pura","Yashu","Ram","Aditi");
        //boolean	allMatch(Predicate<? super T> predicate)
        System.out.println(list.stream().allMatch(s -> s.toLowerCase().contains("a")));
        //boolean	anyMatch(Predicate<? super T> predicate)
        System.out.println(list.stream().anyMatch(s->s.toLowerCase().contains("y")));
        //long	count()
        System.out.println(list.stream().count());
        //Stream<T>	distinct()
        list.stream().distinct().forEach(System.out::println);
        //Stream<T>	filter(Predicate<? super T> predicate)
        list.stream().filter(s->s.length()>3).forEach(System.out::println);
        //Optional<T>	findAny()
        list.stream().filter(s->s.length()>3).findAny();
        //Optional<T>	findFirst()
        list.stream().filter(s->s.startsWith("A")).findFirst();
        //static <T> Stream<T>	generate(Supplier<T> s)
        //Stream<T>	limit(long maxSize)
        Stream.generate(()->10).limit(8).forEach(System.out::println);
        //map  <R> Stream<R>	map(Function<? super T,? extends R> mapper)
        list.stream().map((s)->s.toUpperCase()).forEach(System.out::println);
        //IntStream	mapToInt(ToIntFunction<? super T> mapper)
        //Returns an IntStream consisting of the results of applying the given function to the elements of this stream.
        list.stream().mapToInt(s->s.length()).forEach(System.out::println);
        //DoubleStream	mapToDouble(ToDoubleFunction<? super T> mapper)
        //Returns a DoubleStream consisting of the results of applying the given function to the elements of this stream.
        list.stream().mapToDouble(s->s.length()*2).forEachOrdered(System.out::println);
        //mLongStream	mapToLong(ToLongFunction<? super T> mapper)
        //Returns a LongStream consisting of the results of applying the given function to the elements of this stream.
        System.out.println("****************");
        list.stream().mapToLong(s->s.length()).forEach(System.out::println);
        //Optional<T> max(Comparator<? super T> comparator)
        System.out.println(list.stream().max(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        }).toString());
        //Optional<T>	min(Comparator<? super T> comparator)
        System.out.println(list.stream().min(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        }).toString());
        //static <T> Stream<T>	iterate(T seed, UnaryOperator<T> f)
        //Returns an infinite sequential ordered Stream produced by iterative application of a function f to an initial element seed, producing a Stream consisting of seed, f(seed), f(f(seed)), etc.
        Stream.iterate(15,n->n+n).limit(10).forEach(System.out::println);
        //collect
        //concat
        //of
        //peek
        //reduce
        //skip
        //sorted
        //toArray
    }
}
